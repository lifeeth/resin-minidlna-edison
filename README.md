# Resin Edison USB HOST - MiniDLNA example

## Running the demo

* Provision an Intel Edison by following the instructions at [http://docs.resin.io/#/pages/installing/gettingStarted-Edison.md]()
* “git push” this repo to Resin.
