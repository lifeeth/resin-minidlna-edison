#!/bin/bash

mount -t devtmpfs none /dev
udevd --daemon

dd if=/dev/zero of=/data/blank.img bs=10M count=1

# Edison needs external powering for the USB host mode to be active - Either through the DC jack on the arduino board or through the connector on the smaller board.
# The following is needed to get the Edison to switch to host mode
sync && modprobe g_multi file=/data/blank.img stall=0 idVendor=0x8087 idProduct=0x0A9E iProduct=Edison iManufacturer=Intel

udevadm trigger
udevadm settle

# Shutdown the unnecessary usb0 spawned by g_mutli
sleep 5s && ifconfig usb0 down

# Unmount any existing /dev/sda1 mounts
umount /dev/sda1
mount /dev/sda1 /var/lib/minidlna

minidlnad

while true; do echo 'Running..'; sleep 10; done
