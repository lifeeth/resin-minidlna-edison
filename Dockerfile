FROM resin/i386-debian:jessie

# Install Python.
RUN apt-get update

RUN apt-get install -y minidlna net-tools module-init-tools \
        && apt-get clean \
        && apt-get autoremove -qqy

ADD . /app

RUN chmod a+x /app/start.sh

CMD /app/start.sh
